INSERT INTO role VALUES (1, 'ADMINISTRADOR'), (2, 'USUARIO');

INSERT INTO usuario VALUES (1,'mario7lagoas@gmail.com','$2a$10$2jLw.pBVFdxWsRVq/ECKhuL5JiqrpaIOkXuNGG/En37yHYv/nxbKW', 'Mario Sergio');

INSERT INTO usuarios_roles VALUES (1,1),(1,2);