CREATE TABLE role (
    id bigint not null auto_increment,
    nome enum ('ADMINISTRADOR','USUARIO'),
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tarefa (
    id bigint not null auto_increment,
    criador varbinary(1000) not null,
    data_atualizacao time(6),
    data_cadastro time(6),
    descricao varchar(255) not null,
    quantidade_horas_estimadas integer,
    quantidade_horas_realizada integer,
    responsavel varbinary(1000),
    status enum ('CRIADA','PROGRESSO','BLOQUEADA','FINALIZADA') not null,
    titulo varchar(255) not null,
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuario (
    id bigint not null auto_increment,
    email varchar(50),
    password varchar(150),
    user_name varchar(255),
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuarios_roles (
    usuario_id bigint not null,
    role_id bigint not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE usuario ADD CONSTRAINT UC_UsuarioEmail UNIQUE (email);

ALTER TABLE usuarios_roles ADD CONSTRAINT UC_UsuarioRole UNIQUE (usuario_id, role_id);

ALTER TABLE usuarios_roles ADD CONSTRAINT FK_RoleIdAndIdRole FOREIGN KEY (role_id) REFERENCES role (id);

ALTER TABLE usuarios_roles ADD CONSTRAINT FK_UsuarioIdAndIdUsuario FOREIGN KEY (usuario_id) REFERENCES usuario (id);
