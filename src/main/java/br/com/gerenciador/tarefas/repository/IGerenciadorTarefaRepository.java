package br.com.gerenciador.tarefas.repository;

import br.com.gerenciador.tarefas.entity.TarefaEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface IGerenciadorTarefaRepository extends JpaRepository<TarefaEntity, Long> {
    Page<TarefaEntity> findByTituloContaining(String titulo, Pageable page);
}
