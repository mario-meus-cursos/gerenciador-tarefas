package br.com.gerenciador.tarefas.repository;

import br.com.gerenciador.tarefas.entity.RoleEntity;
import br.com.gerenciador.tarefas.enuns.PermissaoEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepository extends JpaRepository<RoleEntity, Long> {

    RoleEntity findByNome(PermissaoEnum nome);
}
