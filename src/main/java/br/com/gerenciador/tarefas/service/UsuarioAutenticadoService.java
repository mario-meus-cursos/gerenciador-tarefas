package br.com.gerenciador.tarefas.service;

import br.com.gerenciador.tarefas.entity.UsuarioEntity;
import br.com.gerenciador.tarefas.repository.IUsuarioRepository;
import br.com.gerenciador.tarefas.security.UserDetail;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional
public class UsuarioAutenticadoService implements UserDetailsService {

    @Autowired
    private IUsuarioRepository iUsuarioRepository;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        UsuarioEntity usuario = iUsuarioRepository.findByEmail(email)
                .orElseThrow(()-> new UsernameNotFoundException("Email " + email + " não Encontrado!"));

        List<String> roles = usuario.getRoles()
                .stream()
                .map(role -> new String(role.getNome().toString()))
                .collect(Collectors.toList());
        return new UserDetail(usuario.getUserName(),usuario.getEmail(), usuario.getPassword(), roles);
    }
}
