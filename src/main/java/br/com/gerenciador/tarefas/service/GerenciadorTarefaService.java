package br.com.gerenciador.tarefas.service;

import br.com.gerenciador.tarefas.entity.TarefaEntity;
import br.com.gerenciador.tarefas.enuns.TarefaStatusEnum;
import br.com.gerenciador.tarefas.exceptions.NaoPermitidoAlterarTarefaException;
import br.com.gerenciador.tarefas.exceptions.NaoPermitirExcluirTarefaException;
import br.com.gerenciador.tarefas.exceptions.TarefaNaoEncontradaException;
import br.com.gerenciador.tarefas.mapper.TarefaMapper;
import br.com.gerenciador.tarefas.model.request.AtualizarTarefaRequest;
import br.com.gerenciador.tarefas.model.request.CadastroTarefaRequest;
import br.com.gerenciador.tarefas.model.response.AtualizarTarefaResponse;
import br.com.gerenciador.tarefas.model.response.CadastroTarefaResponse;
import br.com.gerenciador.tarefas.model.response.ObterCadastroResponse;
import br.com.gerenciador.tarefas.model.response.ObterTarefasPaginadaResponse;
import br.com.gerenciador.tarefas.repository.IGerenciadorTarefaRepository;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Transactional
public class GerenciadorTarefaService {

    @Autowired
    private IGerenciadorTarefaRepository iGerenciadorTarefaRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private TarefaMapper mapper;

    public CadastroTarefaResponse salvarTarefa(CadastroTarefaRequest cadastroTarefaRequest) {
        TarefaEntity tarefa = TarefaEntity.builder()
                .titulo(cadastroTarefaRequest.getTitulo())
                .descricao(cadastroTarefaRequest.getDescricao())
                .criador(usuarioService.findById(cadastroTarefaRequest.getCriadorId()))
                .quantidadeHorasEstimadas(cadastroTarefaRequest.getQuantidadeHorasEstimadas())
                .status(TarefaStatusEnum.CRIADA)
                .build();

        TarefaEntity newTarefa = iGerenciadorTarefaRepository.save(tarefa);

        return mapper.tarefaToCadastroResponde(newTarefa, newTarefa.getCriador().getUserName());
    }

    public ObterTarefasPaginadaResponse obterTarefas(String titulo, int page, int size) {

        Page<TarefaEntity> tarefasPaginadas = iGerenciadorTarefaRepository.findByTituloContaining(titulo, PageRequest.of(page, size));

        return ObterTarefasPaginadaResponse.builder()
                .paginaAtual(tarefasPaginadas.getNumber())
                .totalPaginas(tarefasPaginadas.getTotalPages())
                .totalItens(tarefasPaginadas.getTotalElements())
                .tarefas(getTarefas(tarefasPaginadas))
                .build();
    }

    private List<ObterCadastroResponse> getTarefas(Page<TarefaEntity> tarefasPaginadas) {

        return tarefasPaginadas.getContent()
                .stream()
                .map(tarefa -> {
                    return ObterCadastroResponse.builder()
                            .id(tarefa.getId())
                            .titulo(tarefa.getTitulo())
                            .descricao(tarefa.getDescricao())
                            .criadorNome(tarefa.getCriador().getUserName())
                            .responsavel(tarefa.getResponsavel() != null ? tarefa.getResponsavel().getUserName() : "NAO ATRIBUÎDO")
                            .status(tarefa.getStatus())
                            .quantidadeHorasEstimadas(tarefa.getQuantidadeHorasEstimadas())
                            .quantidadeHorasRealizada(tarefa.getQuantidadeHorasRealizada())
                            .dataCadastro(tarefa.getDataCadastro())
                            .dataAtualizacao(tarefa.getDataAtualizacao())
                            .build();

                })
                .toList();
    }

    public void deletarTarefa(Long id) {
        if (!iGerenciadorTarefaRepository.findById(id).isPresent())
            throw new TarefaNaoEncontradaException("Tarefa de id " + id + " não encrontrado");

        TarefaEntity tarefaEntity = iGerenciadorTarefaRepository.findById(id).get();

        if (!TarefaStatusEnum.CRIADA.equals(tarefaEntity.getStatus())) {
            throw new NaoPermitirExcluirTarefaException();
        }

        iGerenciadorTarefaRepository.deleteById(id);

    }

    public AtualizarTarefaResponse atualizarTarefa(AtualizarTarefaRequest tarefaRequest, Long id) {

        if (!iGerenciadorTarefaRepository.findById(id).isPresent())
            throw new TarefaNaoEncontradaException("Tarefa de id " + id + " não encrontrado");

        TarefaEntity tarefaAtualizar = iGerenciadorTarefaRepository.findById(id).get();

        if(tarefaAtualizar.getStatus().equals(TarefaStatusEnum.FINALIZADA)){
            throw new NaoPermitidoAlterarTarefaException("Nao permitido Alterar o status de tarefa com status FINALIZADA");
        }

        if(tarefaAtualizar.getStatus().equals(TarefaStatusEnum.CRIADA) && tarefaRequest.getStatus().equals(TarefaStatusEnum.FINALIZADA)){
            throw new NaoPermitidoAlterarTarefaException("Nao permitido Alterar o status da tarefa para FINALIZADA" +
                    " se a mesma estiver com status de CRIADA");
        }

        if(tarefaAtualizar.getStatus().equals(TarefaStatusEnum.BLOQUEADA) && tarefaRequest.getStatus().equals(TarefaStatusEnum.FINALIZADA)){
            throw new NaoPermitidoAlterarTarefaException("Nao permitido Alterar o status da tarefa para FINALIZADA" +
                    " se a mesma estiver com status de BLOQUEADA");
        }


        tarefaAtualizar.setTitulo(tarefaAtualizar.getTitulo());
        tarefaAtualizar.setResponsavel(usuarioService.findById(tarefaRequest.getResponsavelId()));
        tarefaAtualizar.setStatus(tarefaRequest.getStatus());
        tarefaAtualizar.setQuantidadeHorasRealizada(tarefaRequest.getQuantidadeHorasRealizada());
        tarefaAtualizar.setDescricao(tarefaAtualizar.getDescricao());


        TarefaEntity tarefaAlterada = iGerenciadorTarefaRepository.save(tarefaAtualizar);

        return mapper.tarefaToAtualizarResponse(tarefaAlterada,
                tarefaAlterada.getCriador().getUserName(),
                tarefaAlterada.getResponsavel().getUserName());
    }

}
