package br.com.gerenciador.tarefas.service;

import br.com.gerenciador.tarefas.entity.RoleEntity;
import br.com.gerenciador.tarefas.entity.UsuarioEntity;
import br.com.gerenciador.tarefas.exceptions.UsuarioNaoEncontradoException;
import br.com.gerenciador.tarefas.mapper.TarefaMapper;
import br.com.gerenciador.tarefas.model.response.UsuarioCadastroResponse;
import br.com.gerenciador.tarefas.repository.IRoleRepository;
import br.com.gerenciador.tarefas.repository.IUsuarioRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;


@Service
@Transactional
public class UsuarioService {

    @Autowired
    private IUsuarioRepository iUsuarioRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IRoleRepository iRoleRepository;

    @Autowired
    private TarefaMapper mapper;


    public UsuarioCadastroResponse salvar(UsuarioEntity usuario) {

        usuario.setRoles(getRules(usuario));

        usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));

        return mapper.usuarioToUsuarioCadastroResponse(iUsuarioRepository.save(usuario));
    }

    private List<RoleEntity> getRules(UsuarioEntity usuario) {
        return usuario.getRoles()
                .stream()
                .map(role -> iRoleRepository.findByNome(role.getNome()))
                .toList();
    }

    public UsuarioCadastroResponse atualizar(Long usuarioId, UsuarioEntity usuario) {

        findById(usuarioId);

        usuario.setId(usuarioId);
        usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
        usuario.setRoles(getRules(usuario));

        return mapper.usuarioToUsuarioCadastroResponse(iUsuarioRepository.save(usuario));
    }

    public void deletarUsario(Long usuarioId) {

        UsuarioEntity usuario = findById(usuarioId);
        UsuarioEntity usuarioDelete = getUsarioAndRolesDelete(usuario);

        iUsuarioRepository.delete(usuarioDelete);

    }

    private UsuarioEntity getUsarioAndRolesDelete(UsuarioEntity usuario) {
        UsuarioEntity novoUsuario = usuario;
        novoUsuario.getRoles().clear();
        if (!isEmpty(usuario.getRoles())) {
            novoUsuario.getRoles().addAll(usuario.getRoles().stream()
                    .map(role -> {
                        List<UsuarioEntity> usuarios = new ArrayList<>();
                        usuarios.add(usuario);
                        role.setUsuarios(usuarios);
                        return role;
                    })
                    .toList()
            );
        }
        return novoUsuario;
    }

    public List<UsuarioCadastroResponse> gelAll() {
        return mapper.listUsuarioTolistUsuarioResponse(iUsuarioRepository.findAll());
    }

    public UsuarioEntity findById(Long criadorId) {
        if (!iUsuarioRepository.findById(criadorId).isPresent())
            throw new UsuarioNaoEncontradoException("Usuario id " + criadorId + " não encontrado.");

        return iUsuarioRepository.findById(criadorId).get();
    }

    public UsuarioCadastroResponse findByIdResponse(Long usuarioId) {

        return mapper.usuarioToUsuarioCadastroResponse(findById(usuarioId));
    }
}
