package br.com.gerenciador.tarefas;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TarefasApplication {
    public static void main(String[] args) {
        SpringApplication.run(TarefasApplication.class, args);
    }

    @Bean
    public OpenAPI customOpemApi(){
        return new OpenAPI()
                .info(
                        new Info()
                                .title("Gerenciador de Tarefas - Curso")
                                .description("Simples API de Gerenciador de Tarefas")
                                .contact( new Contact()
                                        .email("mario7lagoas@gmail.com")
                                        .name("Mario Sergio"))
                                .version("v1")
                                .license(
                                        new License()
                                                .name("Apache 2.0")
                                )

                );
    }
}
