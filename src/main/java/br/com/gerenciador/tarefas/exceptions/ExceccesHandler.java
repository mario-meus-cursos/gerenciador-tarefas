package br.com.gerenciador.tarefas.exceptions;

import br.com.gerenciador.tarefas.enuns.ErrosEnum;
import br.com.gerenciador.tarefas.model.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class ExceccesHandler {

    @ExceptionHandler(NaoPermitirExcluirTarefaException.class)
    public ResponseEntity<ErrorResponse> naoPermitirExcluirExceptionsHandler(NaoPermitirExcluirTarefaException ex){

        Map<String, String> response = new HashMap<>();
        response.put("codigo", ErrosEnum.NAO_PERMITIDO_EXCLUIR.toString());
        response.put("mensagem", "Não é permitido excluir uma tarefa com status diferente CRIADA");

        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(HttpStatus.UNPROCESSABLE_ENTITY.toString())
                .erros(Collections.singletonList(response))
                .build();

        return new ResponseEntity<>(errorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(TarefaNaoEncontradaException.class)
    public ResponseEntity<ErrorResponse> tarefaNaoEncontradoHandler(TarefaNaoEncontradaException ex){

        Map<String, String> response = new HashMap<>();
        response.put("codigo", ErrosEnum.TAREFA_NAO_ENCONTRADA.toString());
        response.put("mensagem", ex.getMessage());

        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.toString())
                .erros(Collections.singletonList(response))
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

    }
    @ExceptionHandler(NaoPermitidoAlterarTarefaException.class)
    public ResponseEntity<ErrorResponse> naoPermitidoAlterHandler(NaoPermitidoAlterarTarefaException ex){

        Map<String, String> response = new HashMap<>();
        response.put("codigo", ErrosEnum.NAO_PERMITIDO_ALTERAR_STATUS.toString());
        response.put("mensagem", ex.getMessage());

        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.toString())
                .erros(Collections.singletonList(response))
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.UNPROCESSABLE_ENTITY);

    }

    @ExceptionHandler(UsuarioNaoEncontradoException.class)
    public ResponseEntity<ErrorResponse> usuarioNaoEncontradoHandler(UsuarioNaoEncontradoException ex){

        Map<String, String> response = new HashMap<>();
        response.put("codigo", ErrosEnum.USUARIO_NAO_ENCONTRADO.toString());
        response.put("mensagem", ex.getMessage());

        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.toString())
                .erros(Collections.singletonList(response))
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

    }
    @ExceptionHandler(NoResourceFoundException.class)
    public ResponseEntity<ErrorResponse> oResourceFoundHandle(NoResourceFoundException ex) {

        Map<String, String> response = new HashMap<>();
        response.put("codigo", ErrosEnum.ENDPOINT_NAO_ENCONTRADO.toString());
        response.put("ensagem", ex.getBody().toString());

        ErrorResponse errorresponse = ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.toString())
                .erros(Collections.singletonList(response))
                .build();

        return new ResponseEntity<>(errorresponse, HttpStatus.NOT_FOUND);
    }

}
