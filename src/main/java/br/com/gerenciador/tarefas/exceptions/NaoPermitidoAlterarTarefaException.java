package br.com.gerenciador.tarefas.exceptions;

public class NaoPermitidoAlterarTarefaException extends RuntimeException{

    public NaoPermitidoAlterarTarefaException(){
        super();
    }

    public NaoPermitidoAlterarTarefaException(String mensagem){
        super(mensagem);
    }
}
