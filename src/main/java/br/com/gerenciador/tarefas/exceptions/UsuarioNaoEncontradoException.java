package br.com.gerenciador.tarefas.exceptions;

public class UsuarioNaoEncontradoException extends RuntimeException{

    public UsuarioNaoEncontradoException(){
        super();
    }

    public UsuarioNaoEncontradoException(String id){
        super(id);
    }
}
