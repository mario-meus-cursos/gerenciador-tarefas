package br.com.gerenciador.tarefas.exceptions;

public class TarefaNaoEncontradaException extends RuntimeException{

    public TarefaNaoEncontradaException(){
        super();
    }

    public TarefaNaoEncontradaException(String message){
        super(message);
    }
}
