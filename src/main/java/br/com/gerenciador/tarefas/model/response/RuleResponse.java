package br.com.gerenciador.tarefas.model.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RuleResponse {

    private String nome;
}
