package br.com.gerenciador.tarefas.model.request;

import br.com.gerenciador.tarefas.enuns.TarefaStatusEnum;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@Builder
public class CadastroTarefaRequest {

    @NotBlank(message = "{cadastrar.tarefa.request.titulo.obrigatorio}")
    @NotNull(message = "{cadastrar.tarefa.request.titulo.obrigatorio}")
    private String titulo;
    @NotBlank(message = "{cadastrar.tarefa.request.descricao.obrigatorio}")
    @NotNull(message = "{cadastrar.tarefa.request.descricao.obrigatorio}")
    @Length(max = 150 , message = "{cadastrar.tarefa.request.descricao.limite}")
    private String descricao;
    @NotNull(message = "{cadastrar.tarefa.request.criador.obrigatorio}")
    private Long criadorId;
    @NotNull(message = "{cadastrar.tarefa.request.quantidadeHorasEstimadas.obrigatorio}")
    private Integer quantidadeHorasEstimadas;

}
