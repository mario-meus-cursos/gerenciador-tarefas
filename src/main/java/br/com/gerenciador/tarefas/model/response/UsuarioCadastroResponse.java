package br.com.gerenciador.tarefas.model.response;

import br.com.gerenciador.tarefas.model.response.RuleResponse;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
public class UsuarioCadastroResponse {

    private Long id;
    private String userName;
    private String email;
    private List<RuleResponse> roles;
}
