package br.com.gerenciador.tarefas.model.response;

import br.com.gerenciador.tarefas.enuns.TarefaStatusEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CadastroTarefaResponse {
    private Long id;
    private String titulo;
    private String descricao;
    private String criadorNome;
    private TarefaStatusEnum status;

}
