package br.com.gerenciador.tarefas.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioAutenticado {

    private String email;
    private String password;
    private  String userName;

}
