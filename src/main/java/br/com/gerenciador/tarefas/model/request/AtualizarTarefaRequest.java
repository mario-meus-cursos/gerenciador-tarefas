package br.com.gerenciador.tarefas.model.request;

import br.com.gerenciador.tarefas.entity.UsuarioEntity;
import br.com.gerenciador.tarefas.enuns.TarefaStatusEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AtualizarTarefaRequest {

    @NotBlank(message = "{cadastrar.tarefa.request.titulo.obrigatorio}")
    @NotNull(message = "{cadastrar.tarefa.request.titulo.obrigatorio}")
    private String titulo;
    @NotBlank(message = "{cadastrar.tarefa.request.descricao.obrigatorio}")
    @NotNull(message = "{cadastrar.tarefa.request.descricao.obrigatorio}")
    private String descricao;
    @NotNull(message = "{atualizar.tarefa.request.status.obrigatorio}")
    private TarefaStatusEnum status;
    @NotNull(message = "{atualizar.tarefa.request.criador.obrigatorio}")
    private Long responsavelId;
    @NotNull(message = "{cadastrar.tarefa.request.quantidadeHorasEstimadas.obrigatorio}")
    private Integer quantidadeHorasEstimadas;
    private Integer quantidadeHorasRealizada;

}

