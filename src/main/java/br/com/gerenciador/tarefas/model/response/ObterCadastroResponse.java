package br.com.gerenciador.tarefas.model.response;

import br.com.gerenciador.tarefas.enuns.TarefaStatusEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
@Builder
public class ObterCadastroResponse {
    private Long id;
    private String titulo;
    private String descricao;
    private String criadorNome;
    private String responsavel;
    private TarefaStatusEnum status;
    private int quantidadeHorasEstimadas;
    private Integer quantidadeHorasRealizada;
    private LocalTime dataCadastro;
    private LocalTime dataAtualizacao;

}
