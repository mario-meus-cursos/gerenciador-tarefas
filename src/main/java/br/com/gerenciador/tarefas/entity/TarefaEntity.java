package br.com.gerenciador.tarefas.entity;

import br.com.gerenciador.tarefas.enuns.TarefaStatusEnum;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serializable;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name= "tarefa")
public class TarefaEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String titulo;
    @Column(nullable = false)
    private String descricao;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING )
    private TarefaStatusEnum status;

    @Column(nullable = false, length = 1000)
    private UsuarioEntity criador;

    @Column(length = 1000)
    private UsuarioEntity responsavel;

    private Integer quantidadeHorasEstimadas;

    private Integer quantidadeHorasRealizada;


    @CreationTimestamp
    private LocalTime dataCadastro;

    @UpdateTimestamp
    private LocalTime dataAtualizacao;

}
