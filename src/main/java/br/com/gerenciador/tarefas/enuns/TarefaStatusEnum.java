package br.com.gerenciador.tarefas.enuns;

public enum TarefaStatusEnum {
    CRIADA, PROGRESSO, BLOQUEADA, FINALIZADA
}
