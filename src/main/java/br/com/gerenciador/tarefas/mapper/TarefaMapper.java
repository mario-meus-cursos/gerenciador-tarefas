package br.com.gerenciador.tarefas.mapper;

import br.com.gerenciador.tarefas.entity.TarefaEntity;
import br.com.gerenciador.tarefas.entity.UsuarioEntity;
import br.com.gerenciador.tarefas.model.response.AtualizarTarefaResponse;
import br.com.gerenciador.tarefas.model.response.CadastroTarefaResponse;
import br.com.gerenciador.tarefas.model.response.UsuarioCadastroResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface TarefaMapper {
    List<UsuarioCadastroResponse> listUsuarioTolistUsuarioResponse(List<UsuarioEntity> usuarios);

    UsuarioCadastroResponse usuarioToUsuarioCadastroResponse(UsuarioEntity usuario);

    CadastroTarefaResponse tarefaToCadastroResponde(TarefaEntity tarefa, String criadorNome);

    @Mapping(source = "responsavel", target = "responsavel")
    AtualizarTarefaResponse tarefaToAtualizarResponse(TarefaEntity tarefaAlterada,String criadorNome, String responsavel);
}
