package br.com.gerenciador.tarefas.controller;

import br.com.gerenciador.tarefas.model.request.AtualizarTarefaRequest;
import br.com.gerenciador.tarefas.model.request.CadastroTarefaRequest;
import br.com.gerenciador.tarefas.model.response.AtualizarTarefaResponse;
import br.com.gerenciador.tarefas.model.response.CadastroTarefaResponse;
import br.com.gerenciador.tarefas.model.response.ObterTarefasPaginadaResponse;
import br.com.gerenciador.tarefas.service.GerenciadorTarefaService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/gereciador-tarefa")
public class GerenciadorTarefaController {

    @Autowired
    private GerenciadorTarefaService gerenciadorTarefaService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Tarefa cadastrada com sucesso."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @Operation(description = "Retorna tarefas Paginadas")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CadastroTarefaResponse> cadastrarTarefa(@Valid @RequestBody CadastroTarefaRequest request) {

        return new ResponseEntity<>(gerenciadorTarefaService.salvarTarefa(request), HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tarefas encontradas com sucesso."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @Operation(description = "Retorna tarefas Paginadas")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObterTarefasPaginadaResponse> obterCatrados(@RequestParam(name = "page", defaultValue = "0") int page,
                                                                      @RequestParam(name = "size", defaultValue = "3") int size,
                                                                      @RequestParam(name = "titulo", defaultValue = "") String titulo) {

        return new ResponseEntity<>(gerenciadorTarefaService.obterTarefas(titulo, page, size), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Tarefa Excluida com sucesso."),
            @ApiResponse(responseCode = "404", description = "Tarefa não encontrada."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @Operation(description = "Deleta uma tarefa pelo ID.")
    @DeleteMapping(value = "{id}")
    public ResponseEntity deletarTarefa(@PathVariable("id") Long id) {
        gerenciadorTarefaService.deletarTarefa(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Tarefa Alterada com sucesso."),
            @ApiResponse(responseCode = "404", description = "Tarefa não encontrada."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @Operation(description = "Altera uma tarefa.")
    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AtualizarTarefaResponse> atualizarTarefa(@Valid @RequestBody AtualizarTarefaRequest request,
                                                                   @PathVariable("id") Long id) {
        return new ResponseEntity<>(gerenciadorTarefaService.atualizarTarefa(request, id), HttpStatus.ACCEPTED);

    }
}
