package br.com.gerenciador.tarefas.controller;

import br.com.gerenciador.tarefas.entity.UsuarioEntity;
import br.com.gerenciador.tarefas.model.response.UsuarioCadastroResponse;
import br.com.gerenciador.tarefas.service.UsuarioService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;

    @Operation(description = "Cadastrar um novo Usuario.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Criado novo Usuario."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioCadastroResponse> cadastrar(@RequestBody UsuarioEntity usuario) {

        UsuarioCadastroResponse response = usuarioService.salvar(usuario);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Usuario Alterdo com sucesso."),
            @ApiResponse(responseCode = "404", description = "Usuario não encontrado."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @Operation(description = "Altera um Usuario.")
    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioCadastroResponse> alterar(@PathVariable("id") Long usuarioId,
                                                           @RequestBody UsuarioEntity usuario) {

        UsuarioCadastroResponse response = usuarioService.atualizar(usuarioId, usuario);

        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Usuario deletado com sucesso."),
            @ApiResponse(responseCode = "404", description = "Usuario não encontrado."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @Operation(description = "Deleta um Usuario pelo ID.")
    @DeleteMapping(value = "{id}")
    public ResponseEntity deletar(@PathVariable("id") Long usuarioId) {
        usuarioService.deletarUsario(usuarioId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuarios encontrados com sucesso."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @Operation(description = "Retorna uma lista de Usuarios.")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UsuarioCadastroResponse>> getAll() {
        return new ResponseEntity<>(usuarioService.gelAll(), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuario encontrado com sucesso."),
            @ApiResponse(responseCode = "404", description = "Usuario não encontrado."),
            @ApiResponse(responseCode = "403", description = "Não Autorizado.")
    })
    @Operation(description = "Retorna uma Usuario pelo ID.")
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioCadastroResponse> findById(@PathVariable("id") Long usuarioId) {
        return new ResponseEntity<>(usuarioService.findByIdResponse(usuarioId), HttpStatus.OK);
    }
}
